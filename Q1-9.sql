use rezodb;

1
CREATE TABLE item_category (
category_id int AUTO_INCREMENT not null primary key, 
category_name VARCHAR(256) NOT NULL);

2
CREATE TABLE item (
item_id int AUTO_INCREMENT not null primary key, 
item_name VARCHAR(256) NOT NULL,
Item_price int NOT NULL,
category_id int);

3
insert into item_category(category_name)
values("家具");
insert into item_category(category_name)
values("食品");
insert into item_category(category_name)
values("本");

4
insert into item(item_name,Item_price,category_id)
values("堅牢な机",3000,1);
insert into item(item_name,Item_price,category_id)
values("生焼け肉",50,2);
insert into item(item_name,Item_price,category_id)
values("すっきりわかるJava入門",3000,3);
insert into item(item_name,Item_price,category_id)
values("おしゃれな椅子",2000,1);
insert into item(item_name,Item_price,category_id)
values("こんがり肉",500,2);
insert into item(item_name,Item_price,category_id)
values("書き方ドリルSQL",2500,3);
insert into item(item_name,Item_price,category_id)
values("ふわふわのベッド",30000,1);
insert into item(item_name,Item_price,category_id)
values("ミラノ風ドリア",300,2);

5
select item_name,Item_price
from item
where category_id=1;

6
select item_name,Item_price
from item
where Item_price>=1000;

7
select item_name,Item_price
from item
where item_name like "%肉%";

8
select item_id,item_name,Item_price,category_name
from item 
INNER JOIN item_category
on item.category_id = item_category.category_id;

9
select category_name,SUM(Item_price) as "total_price"
from item 
INNER JOIN item_category
on item.category_id = item_category.category_id
GROUP BY item.category_id
ORDER BY total_price DESC
;